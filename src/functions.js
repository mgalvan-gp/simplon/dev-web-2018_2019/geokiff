/* Revoir fonction */
export function axiosFunction(country, posX, scndPosX, posY, scndPosY) {
    posX -= 277;
    scndPosX -= 277;

    const axios = require('axios');
    // api rest url
    axios.get("https://restcountries.com/v3.1/name/" + country)

    .then(function(response) {
        // Init elements
        let title = document.createElement('h5');
        title.className = "title text-center";
        let ulInfos = document.createElement('ul');
        ulInfos.className = "card-body";
        let liCapital = document.createElement('li');
        let liPopulation = document.createElement('li');
        let liMoney = document.createElement('li');
        let liLongLat = document.createElement('li');
        let liArea = document.createElement('li');
        let liRegion = document.createElement('li');
        let liFlag = document.createElement('li');
        liFlag.className = "text-center";
        let imgFlag = document.createElement('img');
        imgFlag.className = "imgFlag";
        let liPeopleName = document.createElement('li');
        let playground = document.querySelector('#playground');
        let infos = document.querySelector('#infos');

        // Mousemove event to show countries informations
        playground.addEventListener("mousemove", function(e) {
            if ((e.clientX >= posX + playground.offsetLeft && e.clientX <= scndPosX + playground.offsetLeft) && (e.clientY >= posY && e.clientY <= scndPosY)) {

                for (const iterator of response.data) {
                    infos.innerHTML = "";

                    playground.appendChild(infos);
                    infos.appendChild(ulInfos);
                    ulInfos.append(title, liCapital, liPeopleName, liPopulation,
                        liMoney, liLongLat, liArea, liRegion, liFlag);
                    liFlag.appendChild(imgFlag);
                    imgFlag.setAttribute("src", iterator.flags.png);
                    title.innerHTML = iterator.name.common;
                    liCapital.innerHTML = "Capitale : " + iterator.capital;
                    liPeopleName.innerHTML = "Habitant(e)s : " + iterator.demonyms.fra.m + "/" + iterator.demonyms.fra.f;
                    liPopulation.innerHTML = "Population : " + iterator.population;
                    liLongLat.innerHTML = "Lattitude,Longitude : " + iterator.latlng;
                    liArea.innerHTML = "Superficie : " + iterator.area + " km²";
                    liRegion.innerHTML = "Continent : " + iterator.subregion;
                }
            }
            console.log(e);

        })
    })


    .catch(function(error) {
        console.log(error);
    })
}